import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CambioMonedaComponent } from './pages/cambio-moneda/cambio-moneda.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'cambio', component: CambioMonedaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
