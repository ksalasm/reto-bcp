import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { UsuarioService } from 'src/app/_service/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;

  constructor(private usuariService: UsuarioService,
     private formBuilder: FormBuilder,
     private router: Router) { }
  
  ngOnInit(): void {
    localStorage.clear();
    this.form = this.formBuilder.group({
      usuario: ['', Validators.required],
      clave: ['', Validators.required],
    });
  }

  get f() { return this.form.controls; }

  ingresar() {
    if (this.form.invalid) return;

    let usuario = this.f.usuario.value;
    let clave = this.f.clave.value;
    this.usuariService.getLogin(usuario,clave).pipe(tap(s => {
      if(s){
        localStorage.setItem('token', s.token);
      }
    })).subscribe(s => {
      if (s) {
        this.router.navigateByUrl('/cambio');
      } else {
        this.router.navigateByUrl('/login');
      }
    });   
  }

}
