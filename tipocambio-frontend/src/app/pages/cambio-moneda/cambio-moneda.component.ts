import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Divisa } from 'src/app/_model/divisa';
import { DivisaService } from 'src/app/_service/divisa.service';
import { UsuarioService } from 'src/app/_service/usuario.service';

@Component({
  selector: 'app-cambio-moneda',
  templateUrl: './cambio-moneda.component.html',
  styleUrls: ['./cambio-moneda.component.css']
})
export class CambioMonedaComponent implements OnInit {

  form!: FormGroup;
  resultado!: string;
  listaDivisas: Divisa[] = [];
  listaMonedasOrigen: string[] = [];
  listaMonedasDestino: string[] = [];

  constructor(private divisaService: DivisaService,
              private formBuilder: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    this.resultado = "";
    this.cargaListaDivisas();

    this.form = this.formBuilder.group({
      'monedaOrigen': ['0', Validators.required],
      'monedaDestino': ['0', Validators.required],
      'monto': ['', Validators.required]
    })
  }

  get f() { return this.form.controls; }

  cargaListaDivisas() {
    this.divisaService.listarDivisas().subscribe(s => { 
      if (s) {
        this.listaDivisas = s;
        this.listaMonedasOrigen = [ ... new Set(s.map(x => x.monedaOrigen))];
        this.listaMonedasDestino = [ ... new Set(s.map(x => x.monedaOrigen))];
      }
    });
  }


  convertir(){
    if(this.form.invalid) return;

    let monedaOrigen = this.f.monedaOrigen.value;
    let monedaDestino = this.f.monedaDestino.value;
    let monto = this.f.monto.value

    console.log('monedaOrigen: ', monedaOrigen)
    console.log('monedaDestino: ', monedaDestino)
    console.log('monto: ', monto)

    this.divisaService.cambioMoneda(monto, monedaOrigen, monedaDestino).subscribe( s => {
      if (s) {
        this.resultado = `El monto destino es: ${s.montoDestino} (${s.monedaDestino}), tipo de cambio: ${s.tipoCambio}`;
      }
    });
  }

  onChangeMontoOrige(){
    let monedaOrigen = this.f.monedaOrigen.value;
    if ('1' === monedaOrigen)
      return;

    this.listaMonedasDestino = this.listaDivisas.filter(x => monedaOrigen === x.monedaOrigen).map(x => x.monedaDestino);
  }

  onChangeMontoDestino() {
    let monedaDestino = this.f.monedaDestino.value;
    if ('1' === monedaDestino)
      return;

    this.listaMonedasOrigen = this.listaDivisas.filter(x => monedaDestino === x.monedaDestino).map(x => x.monedaOrigen);
  }

  salir(){
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }
}
