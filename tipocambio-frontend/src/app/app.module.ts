import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {PanelModule} from 'primeng/panel';
import { LoginComponent } from './pages/login/login.component';
import { CambioMonedaComponent } from './pages/cambio-moneda/cambio-moneda.component';
import { ReactiveFormsModule } from '@angular/forms';
import {CardModule} from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './_service/auth-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CambioMonedaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    PanelModule,
    ReactiveFormsModule,
    CardModule,
    ButtonModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
