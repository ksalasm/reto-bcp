import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Divisa } from '../_model/divisa';
import { Usuario } from '../_model/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private url: string = `${environment.HOST}/admin`;

  constructor(private http: HttpClient) { }

  getLogin(usuario: string, clave: string) {
    const parametros = new HttpParams()
    .set("user", usuario)
    .set("password", clave);
    return this.http.post<Usuario>(`${this.url}`, parametros, { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') });
  }
}
