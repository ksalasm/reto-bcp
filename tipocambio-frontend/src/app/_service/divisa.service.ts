import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Divisa } from '../_model/divisa';
import { Cambio } from '../_model/cambio';

@Injectable({
  providedIn: 'root'
})
export class DivisaService {
   
  private url: string = `${environment.HOST}/bcp/api`;

  constructor(private http: HttpClient) { }

  listarDivisas() {
    return this.http.get<Divisa[]>(this.url);
  }

  cambioMoneda(monto: number, monedaOrigen: string, monedaDestino: string) {
    const params = new HttpParams()
    .set("montoOrigen", monto)
    .set("monedaOrigen", monedaOrigen)
    .set("monedaDestino", monedaDestino);
    return this.http.get<Cambio>(`${this.url}/cambioMoneda`, {params});
  }
}

 
