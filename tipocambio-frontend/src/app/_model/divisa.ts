export class Divisa {
    id!: number;
    monedaOrigen!: string;
    monedaDestino!: string;
    tipoCambio!: number;
}