export class Cambio {
    id!: number;
    montoOrigen!: number;
    montoDestino!: number;
    monedaOrigen!: string;
    monedaDestino!: string;
    tipoCambio!: number;
}