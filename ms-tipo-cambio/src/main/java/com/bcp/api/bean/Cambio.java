package com.bcp.api.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Cambio implements Serializable{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private Long id;
    private BigDecimal montoOrigen;
    private BigDecimal montoDestino;
    private String monedaOrigen;
    private String monedaDestino;
    private BigDecimal tipoCambio;

    
    
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public BigDecimal getMontoOrigen() {
		return montoOrigen;
	}



	public void setMontoOrigen(BigDecimal montoOrigen) {
		this.montoOrigen = montoOrigen;
	}



	public BigDecimal getMontoDestino() {
		return montoDestino;
	}



	public void setMontoDestino(BigDecimal montoDestino) {
		this.montoDestino = montoDestino;
	}



	public String getMonedaOrigen() {
		return monedaOrigen;
	}



	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}



	public String getMonedaDestino() {
		return monedaDestino;
	}



	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}



	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}



	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}



	@Override
	public String toString() {
		return "Cambio [id=" + id + ", montoOrigen=" + montoOrigen + ", montoDestino=" + montoDestino
				+ ", monedaOrigen=" + monedaOrigen + ", monedaDestino=" + monedaDestino + ", tipoCambio=" + tipoCambio
				+ "]";
	}
	
}
