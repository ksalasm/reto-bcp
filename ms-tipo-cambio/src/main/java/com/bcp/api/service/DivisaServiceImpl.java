package com.bcp.api.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.api.bean.Cambio;
import com.bcp.api.model.Divisa;
import com.bcp.api.repository.IDivisaRepository;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;

@Service
public class DivisaServiceImpl implements IDivisaService {
	
	private static final Logger logger = LoggerFactory.getLogger(DivisaServiceImpl.class);
	
	@Autowired
	private IDivisaRepository iDivisaRepo;

	@Override
	public Observable<Divisa> listarDivisas() {
		return Observable.create(observerSuscriber -> {
			try {
				List<Divisa> lista = iDivisaRepo.findAll();
				if (!lista.isEmpty()) {
					lista.stream().forEach(divisa -> observerSuscriber.onNext(divisa));
				}
				observerSuscriber.onComplete();
			} catch (Exception e) {
				observerSuscriber.onError(e);
			}
		});
	}

	@Override
	public Single<Divisa> registrarDivisa(Divisa divisa) {
		return Single.create(s -> s.onSuccess(iDivisaRepo.save(divisa)));
	}

	@Override
	public Single<Cambio> cambioMoneda(Cambio cambio) {
		Divisa divisa = iDivisaRepo.findByMonedaOrigenAndMonedaDestino(cambio.getMonedaOrigen(), cambio.getMonedaDestino());
		return Single.create(subscriber -> {
			try {
				if (divisa != null) {
					BigDecimal montoDestino = cambio.getMontoOrigen().multiply(divisa.getTipoCambio());
					cambio.setTipoCambio(divisa.getTipoCambio());
					cambio.setMontoDestino(montoDestino.setScale(2, RoundingMode.HALF_UP));
					subscriber.onSuccess(cambio);
				}
			} catch (Exception e) {
				subscriber.onError(e);
			}
		});
		
		
	}

	@Override
	public Single<String> cargaDivisasPorDefecto() {
		return Single.create(subscriber -> {
			try {
				List<Divisa> listaDivisas = Arrays.asList(new Divisa("PEN", "USD", new BigDecimal(0.25)),
						new Divisa("PEN", "EUR", new BigDecimal(0.22)),
						new Divisa("USD", "PEN", new BigDecimal(4.05)),
						new Divisa("USD", "EUR", new BigDecimal(0.88)),
						new Divisa("EUR", "PEN", new BigDecimal(4.56)),
						new Divisa("EUR", "USD", new BigDecimal(1.13))
						);
				iDivisaRepo.saveAll(listaDivisas);
				subscriber.onSuccess("Se cargaron las divisas con exito..!");
			} catch (Exception e) {
				subscriber.onError(e);
			}
		});
	}
	
	
}
