package com.bcp.api.service;

import com.bcp.api.bean.Cambio;
import com.bcp.api.model.Divisa;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;

public interface IDivisaService {
	
	Observable<Divisa> listarDivisas();
	Single<Divisa> registrarDivisa(Divisa divisa);
	Single<Cambio> cambioMoneda(Cambio cambio);
	Single<String> cargaDivisasPorDefecto();
}
