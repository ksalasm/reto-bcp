package com.bcp.api.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "divisa",
	  indexes = {@Index(name = "i_divisa", columnList = "moneda_origen,moneda_destino", unique = true)})
public class Divisa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "moneda_origen", nullable = false)
    private String monedaOrigen;

    @Column(name = "moneda_destino", nullable = false)
    private String monedaDestino;

    @Column
    private BigDecimal tipoCambio;
    
	public Divisa() {
	}

	public Divisa(String monedaOrigen, String monedaDestino, BigDecimal tipoCambio) {
		this.monedaOrigen = monedaOrigen;
		this.monedaDestino = monedaDestino;
		this.tipoCambio = tipoCambio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Override
	public String toString() {
		return "Divisa [id=" + id + ", monedaOrigen=" + monedaOrigen + ", monedaDestino=" + monedaDestino
				+ ", tipoCambio=" + tipoCambio + "]";
	}
}
