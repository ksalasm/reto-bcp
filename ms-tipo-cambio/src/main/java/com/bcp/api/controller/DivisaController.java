package com.bcp.api.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.api.bean.Cambio;
import com.bcp.api.model.Divisa;
import com.bcp.api.service.IDivisaService;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;

@RestController
@RequestMapping(value = "/bcp/api")
public class DivisaController {
	
	@Autowired
	private IDivisaService iDivisaService;
	
	
	@GetMapping(value = "/cargarDivisas")
    public Single<ResponseEntity<String>> cargaDivisas() throws Exception {
        return iDivisaService.cargaDivisasPorDefecto().map(respuesta -> new ResponseEntity<>(respuesta, HttpStatus.OK));
    }
	
	@GetMapping()
    public Observable<Divisa> listarDivisas() throws Exception {
        return iDivisaService.listarDivisas();
    }

	@PostMapping()
    public Single<Divisa> registrarDivisa(@RequestBody Divisa divisa) throws Exception {
        return iDivisaService.registrarDivisa(divisa);
    }
	
	@PostMapping(value = "/cambio", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
    public Single<ResponseEntity<Cambio>> cambio(@RequestBody Cambio cambio) throws Exception {
        return iDivisaService.cambioMoneda(cambio).map( respuesta -> new ResponseEntity<>(respuesta, HttpStatus.OK));
    }
	
	@GetMapping(value = "/cambioMoneda", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
    public Single<ResponseEntity<Cambio>> cambioMoneda(@RequestParam("montoOrigen") BigDecimal montoOrigen, @RequestParam("monedaOrigen") String monedaOrigen, @RequestParam("monedaDestino") String monedaDestino) throws Exception {
		Cambio cambio = new Cambio();
		cambio.setMontoOrigen(montoOrigen);
		cambio.setMonedaOrigen(monedaOrigen);
		cambio.setMonedaDestino(monedaDestino);
        return iDivisaService.cambioMoneda(cambio).map( respuesta -> new ResponseEntity<>(respuesta, HttpStatus.OK));
    }
	
	
	
	
	
}
