package com.bcp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcp.api.model.Divisa;

@Repository
public interface IDivisaRepository extends JpaRepository<Divisa, Long>{
	
	Divisa findByMonedaOrigenAndMonedaDestino(String monedaOrigen, String monedaDestino);
}
